var express = require('express');
var amqp = require('amqplib/callback_api');
var uuid = require('node-uuid');
var app = express();

var ch, q;
var cacheRequest={};

amqp.connect('amqp://localhost', function(err, conn) {
  	if (err) {
		console.log("********************");
  		console.log(err);
  		console.log("Errore nella connessione!");
  		return;
  	}
	conn.createChannel(function(err, channel) {
		ch=channel;
	    ch.assertQueue('', {exclusive: true, autoDelete:true}, function(err, queuex) {
	    	q=queuex;
	      	ch.consume(q.queue, function(msg) {

			var stringToRequest = msg.properties.correlationId;
			var objResult = JSON.parse(msg.content.toString());
			console.log("From RabbitMQ");
			var requestList = cacheRequest[stringToRequest];
			cacheRequest[stringToRequest] = null;
			requestList.forEach(function(res) {
				res.end( "Result sum (1+2): "+objResult.Total);
			});

	      }, {noAck: true});
	      
	    });
	});
});

app.get('/', function (req, res) {
	var objToRequest = {Number1:1, Number2:2};
	var stringToRequest= JSON.stringify(objToRequest);
	console.log("********* Request: "+stringToRequest);

	if (!cacheRequest[stringToRequest]) {
		console.log("Request new");
		cacheRequest[stringToRequest]=[];
		ch.publish('ExchangeIntegerAddition', '', new Buffer(stringToRequest),
			{ correlationId: stringToRequest, replyTo: q.queue });	
}
	else {
		console.log("Request cached");
	}
	cacheRequest[stringToRequest].push(res);
});

// Handle 404
app.use(function(req, res) {
 res.status(404).send('404: Page not Found');
});

// Handle 500
app.use(function(error, req, res, next) {
 res.status(500).send('500: Internal Server Error');
});

var server = app.listen(8001, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Example app listening at http://%s:%s", host, port)

})

