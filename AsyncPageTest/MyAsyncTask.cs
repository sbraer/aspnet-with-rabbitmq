﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Web;

namespace AsyncPageTest
{
    public class MyAsyncTask
    {
        public String _taskprogress;
        public AsyncTaskDelegate _dlgt;

        // Create delegate.
        public delegate void AsyncTaskDelegate();
        BlockingCollection<string> cicciociccio = new BlockingCollection<string>();
        BufferBlock<string> cc = new BufferBlock<string>();

        public String GetAsyncTaskProgress()
        {
            return _taskprogress;
        }
        public void DoTheAsyncTask()
        {
            // Introduce an artificial delay to simulate a delayed 
            // asynchronous task. Make this greater than the 
            // AsyncTimeout property.
            Thread.Sleep(TimeSpan.FromSeconds(0.1));
            Task.Factory.StartNew(() => Thread.Sleep(3 * 1000))
            .ContinueWith((t) =>
            {
                //cicciociccio.Add(DateTime.Now.ToString());
                cc.SendAsync("eccome qua");
            }, TaskScheduler.Current);

            //string gg = cicciociccio.Take();
            //_taskprogress += " zio porcoi '"+gg+"' ";
            Task<string> bau= cc.ReceiveAsync();
            _taskprogress += " await: '"+ bau.Result+"' ";
        }

        // Define the method that will get called to
        // start the asynchronous task.
        public IAsyncResult OnBegin(object sender, EventArgs e,  AsyncCallback cb, object extraData)
        {
            _taskprogress = "Beginning async task.";

            _dlgt = new AsyncTaskDelegate(DoTheAsyncTask);
            IAsyncResult result = _dlgt.BeginInvoke(cb, extraData);

            return result;
        }

        // Define the method that will get called when
        // the asynchronous task is ended.
        public void OnEnd(IAsyncResult ar)
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            _taskprogress += " Asynchronous task completedx.";
            _dlgt.EndInvoke(ar);
        }

        // Define the method that will get called if the task
        // is not completed within the asynchronous timeout interval.
        public void OnTimeout(IAsyncResult ar)
        {
            _taskprogress = "Ansynchronous task failed to complete " +
                "because it exceeded the AsyncTimeout parameter.";
        }
    }
}