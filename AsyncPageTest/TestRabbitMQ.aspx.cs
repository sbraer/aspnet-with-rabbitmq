﻿using MicroserviceAddiction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks.Dataflow;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AsyncPageTest
{
    public partial class TestRabbitMQ : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            AdditionServiceClass value;

            var objToRequest = new AdditionServiceClass { Number1=5, Number2=2 };

            try
            {
                value = await RabbitUtilityClass.SendRequest(objToRequest);
                msg.Text = value.Total.ToString();
            }
            catch
            {
                msg.Text = "Timeout";
            }
        }
    }
}