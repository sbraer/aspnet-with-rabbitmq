﻿using MicroserviceAddiction;
using RabbitMQ.Client;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Web;
using System.Web.Script.Serialization;

namespace AsyncPageTest
{
    public class RabbitUtilityClass
    {
        const string ExchangeName = "ExchangeIntegerAddition";

        static ConnectionFactory _connectionFactory;
        static IConnection _connection;
        static IModel _modelCentralized;
        static string _queueName;

        //static ConcurrentDictionary<string, BufferBlock<AdditionServiceClass>> _internalRequest = new ConcurrentDictionary<string, BufferBlock<AdditionServiceClass>>();
        static ConcurrentDictionary<string, List<BufferBlock<AdditionServiceClass>>> _cacheRequest = new ConcurrentDictionary<string, List<BufferBlock<AdditionServiceClass>>>();
        static JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

        static RabbitUtilityClass()
        {
            _connectionFactory = new ConnectionFactory();
            _connectionFactory.HostName = "localhost";
            _connection = _connectionFactory.CreateConnection();

            _modelCentralized = _connection.CreateModel();
            var queueResult = RabbitUtilityClass._modelCentralized.QueueDeclare("", false, true, true, null);
            _queueName = queueResult.QueueName;
            RabbitUtilityClass._modelCentralized.BasicQos(0, 1, false);

            QueueingBasicConsumer consumer = new QueueingBasicConsumer(RabbitUtilityClass._modelCentralized);
            string consumerTag = RabbitUtilityClass._modelCentralized.BasicConsume(_queueName, false, consumer);

            Task.Run(() =>
            {
                while (true)
                {
                    var e = (RabbitMQ.Client.Events.BasicDeliverEventArgs)consumer.Queue.Dequeue();
                    string content = Encoding.Default.GetString(e.Body);
                    string messageId = e.BasicProperties.MessageId;
                    string correlationId = e.BasicProperties.CorrelationId;

                    //var reply = _internalRequest[messageId];
                    //reply.Post(jsonSerializer.Deserialize<AdditionServiceClass>(content));

                    var reply = _cacheRequest[correlationId];
                    reply.ForEach(t => t.Post(jsonSerializer.Deserialize<AdditionServiceClass>(content)));
                    _cacheRequest.TryRemove(correlationId, out reply);

                    //_internalRequest.TryRemove(messageId, out reply);
                    RabbitUtilityClass._modelCentralized.BasicAck(e.DeliveryTag, false);
                }
            });
        }
        public static Task<string> SendRequestTest()
        {
            var result = new BufferBlock<string>();
            result.Post("XXX");
            return result.ReceiveAsync<string>((new CancellationTokenSource(3000)).Token);
        }

        public static Task<AdditionServiceClass> SendRequest(AdditionServiceClass objRequested)
        {
            var objJson = jsonSerializer.Serialize(objRequested);
            var result = new BufferBlock<AdditionServiceClass>();

            if (_cacheRequest.ContainsKey(objJson))
            {
                _cacheRequest[objJson].Add(result);
                return result.ReceiveAsync<AdditionServiceClass>((new CancellationTokenSource(30000)).Token);
            }

            _cacheRequest[objJson] = new List<BufferBlock<AdditionServiceClass>>();
            _cacheRequest[objJson].Add(result);

            IBasicProperties basicProperties = _modelCentralized.CreateBasicProperties();
            basicProperties.MessageId = Guid.NewGuid().ToString();
            basicProperties.CorrelationId = objJson;
            basicProperties.ReplyTo = _queueName;

            _modelCentralized.BasicPublish(ExchangeName, "", basicProperties, Encoding.Default.GetBytes(objJson));
            //_internalRequest[basicProperties.MessageId] = result;
            return result.ReceiveAsync<AdditionServiceClass>((new CancellationTokenSource(30000)).Token);
        }
    }
}