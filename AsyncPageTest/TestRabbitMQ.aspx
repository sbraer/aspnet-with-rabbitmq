﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestRabbitMQ.aspx.cs" Inherits="AsyncPageTest.TestRabbitMQ" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Message from RabbitMQ: <asp:Label ID="msg" runat="server" />
    </div>
    </form>
</body>
</html>
