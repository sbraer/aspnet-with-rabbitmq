﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace MicroserviceAddiction
{
    class Program
    {
        const string ExchangeName = "ExchangeIntegerAddition";
        const string QueueName = "QueueIntegerAddition";
        static void Main(string[] args)
        {
            var jsonSerializer = new JavaScriptSerializer();
            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";

            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();
                ModelCentralized.QueueDeclare(QueueName, false, false, true, null);
                ModelCentralized.ExchangeDeclare(ExchangeName, ExchangeType.Direct, false, true, null);
                ModelCentralized.QueueBind(QueueName, ExchangeName, "");

                QueueingBasicConsumer consumer = new QueueingBasicConsumer(ModelCentralized);
                string consumerTag = ModelCentralized.BasicConsume(QueueName, false, consumer);

                Console.WriteLine("Wait incoming addition...");

                while (true)
                {
                    var e = (RabbitMQ.Client.Events.BasicDeliverEventArgs)consumer.Queue.Dequeue();
                    IBasicProperties props = e.BasicProperties;
                    string replyQueue = props.ReplyTo;
                    string correlationId = props.CorrelationId;
                    string messageId = props.MessageId ?? "";

                    string content = Encoding.Default.GetString(e.Body);
                    Console.WriteLine("> {0}", content);

                    var calculationObj = jsonSerializer.Deserialize<AdditionServiceClass>(content);

                    calculationObj.Total = calculationObj.Number1 + calculationObj.Number2;
                    var resultJSON = jsonSerializer.Serialize(calculationObj);

#if(DEBUG)
                    Thread.Sleep(5000);
#endif

                    Console.WriteLine("< {0}", resultJSON);
                    var msgRaw = Encoding.Default.GetBytes(resultJSON);
                    IBasicProperties basicProperties = ModelCentralized.CreateBasicProperties();
                    basicProperties.CorrelationId = correlationId;
                    basicProperties.MessageId = messageId;
                    ModelCentralized.BasicPublish("", replyQueue, basicProperties, msgRaw);

                    ModelCentralized.BasicAck(e.DeliveryTag, false);
                }
            }
        }
    }
}
