﻿using System;

namespace MicroserviceAddiction
{
    public class AdditionServiceClass
    {
        public int Number1 { get; set; }
        public int Number2 { get; set; }
        public int Total { get; set; }
    }
}
